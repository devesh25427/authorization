.PHONY: all build gen-go-proto gen ui
CURDIR:=${PWD}
EXE=
ifeq ($(OS), Windows_NT)
	EXE=.exe
endif
SWAGGERHOSTNAME=cp.devesh25427.io

#For cloudrun
DEFAULTENDPOINT=cpdefault.endpoints.myproject.cloudrun.cloud.goog
export FULLSERVICEDOMAIN ?= $(FULLDOMAIN)
#generate code inside docker
docker-code-gen:
		docker build -t authorization_gen -f Dockerfile.gen .
		rm -rf generated/*
		docker run -v ${CURDIR}/generated/:/authorization/generated authorization_gen


gen:  gen-go-proto
		@rm -rf ./generated


gen-go-proto:
		@mkdir -p ./generated
		@prototool generate || true
		@cp -r ./generated/gitlab.com/devesh25427/authorization/apis/authorization/v2/* ./generated/authorization/v2/

coverage:
		mkdir -p build
		go test -race -v -coverprofile build/coverage.out ./...
		go tool cover -html=build/coverage.out -o build/coverage.html

##Make local docker image
docker-cp:
	docker build -t authorization --target run .
#Run service
docker-cp-run:
	docker run -it -p 8080:8080 authorization

#CLI
build-cli:
	go build -o cmd/authz/authz$(EXE) cmd/authz/main.go
