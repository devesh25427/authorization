/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	_context "context"
	"fmt"
	"net"
	"net/http"

	"github.com/go-sql-driver/mysql"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"github.com/soheilhy/cmux"
	cp "gitlab.com/devesh25427/authorization/apis/authorization/v2"
	"gitlab.com/devesh25427/authorization/pkg/cpserver"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"github.com/spf13/cobra"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		return serve()
	},
}

const (
	defaultPort = 8080
)

var (
	payshieldFQDN     string
	projectId         string
	debug             bool
	host              string
	port              int
	listGrpc          net.Listener
	sbServer          *cpserver.Server
	dbCheck           bool
	dbCheckTimes      int
	dbDialect, dbConn string
)

var mysqlConfig = &mysql.Config{
	User:                 "root",
	Net:                  "tcp",
	Addr:                 "cp-db",
	DBName:               "cp",
	AllowNativePasswords: true,
}

func init() {
	rootCmd.AddCommand(serveCmd)

	/*
		serveCmd.Flags().BoolVar(&dbCheck, "dbcheck", true, "check and wait for db to be active then exit (for init container)")
		serveCmd.Flags().IntVar(&dbCheckTimes, "dbcheck-retries", 20, "Number of times to retry connecting to the DB server.")
		serveCmd.Flags().StringVar(&payshieldFQDN, "ps-host", "testdata/RSAClientCert.p12", "Path to Client Cert bundle in p12 format")
		serveCmd.Flags().StringVar(&clientCert, "client-cert", "testdata/RSAClientCert.p12", "Path to Client Cert bundle in p12 format")
		serveCmd.Flags().StringVar(&clientCertPassword, "client-cert-pw", "", "Password for password protected p12 files.")
		serveCmd.Flags().StringVar(&dbDialect, "db-dialect", "sqlite3", "Gorm supported SQL database dialect")
		serveCmd.Flags().StringVar(&dbConn, "db-conn", "file::memory:?cache=shared", "SQL database connection")
		serveCmd.Flags().StringVar(&mysqlConfig.User, "mysql-username", "root", "MySQL Username")
		serveCmd.Flags().StringVar(&mysqlConfig.Passwd, "mysql-password", "", "MySQL Password")
		serveCmd.Flags().StringVar(&mysqlConfig.Addr, "mysql-address", "slingshot-db:3306", "MySQL Address as host:port ")
		serveCmd.Flags().StringVar(&mysqlConfig.DBName, "mysql-dbname", "slingshot", "MySQL Password")
		serveCmd.Flags().BoolVar(&mysqlConfig.ParseTime, "parse-time", true, "MySQL ParseTime")
	*/
}

func grpcServe(gl net.Listener) (err error) {

	// Google Publsub
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(func(ctx _context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
			logrus.Infof("Path: %s", info.FullMethod)
			return handler(ctx, req)
		}),
	}

	gs := grpc.NewServer(serverOptions...)
	reflection.Register(gs)
	cp.RegisterLoginServer(gs, sbServer)
	cp.RegisterProtectedServiceServer(gs, sbServer)

	return gs.Serve(gl)
}

func httpServe(l net.Listener) (err error) {
	mux := http.NewServeMux()

	//runtime.HTTPError = CustomHTTPError

	// Add API Server
	var rmux http.Handler

	rtmux := runtime.NewServeMux()

	_ = cp.RegisterLoginHandlerServer(_context.Background(), rtmux, sbServer)
	_ = cp.RegisterProtectedServiceHandlerServer(_context.Background(), rtmux, sbServer)

	rmux = rtmux
	// apis
	mux.Handle("/v2/", rmux)
	mux.Handle("/v1/", rmux)
	mux.HandleFunc("/healthy", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(200)
	})
	// asset files
	mux.Handle("/", http.FileServer(http.Dir("./ui")))

	s := &http.Server{Handler: mux}

	return s.Serve(l)
}

func serve() (err error) {
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if sbServer, err = cpserver.NewServer(); err != nil {
		fmt.Printf("Failed to create server")
		return
	}

	addr := fmt.Sprintf("%s:%d", host, port)
	if listGrpc, err = net.Listen("tcp", addr); err != nil {
		fmt.Printf("Failed to listen on %s:%d \n", host, port)
		return
	}

	m := cmux.New(listGrpc)

	grpcListener := m.Match(cmux.HTTP2HeaderField("content-type", "application/grpc"))
	httpListener := m.Match(cmux.HTTP1Fast())

	// Frontend API/UI services
	g := new(errgroup.Group)
	g.Go(func() error { return grpcServe(grpcListener) })
	g.Go(func() error { return httpServe(httpListener) })
	g.Go(func() error { return m.Serve() })
	logrus.Infof("Listening on: http://localhost:%d", port)
	if err = g.Wait(); err != nil {
		logrus.Panic(err)
	}
	return
}
