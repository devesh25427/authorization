package password

import (
	"errors"
	"fmt"

	"gitlab.com/devesh25427/authorization/pkg/users"
)

//Implements password auth
type PasswordAuth struct {
	//password auth configuration if any
}

type PasswordAuthInput struct {
	Password string
}

func (p PasswordAuth) AuthenticateUser(u *users.User, i interface{}) (err error) {
	fmt.Printf("Password authetications")
	var in *PasswordAuthInput
	if t, ok := i.(PasswordAuthInput); ok {
		in = &t
	} else {
		fmt.Printf("Password authetications : Invalid input")
		err = errors.New("invalid input type")
		return
	}
	fmt.Printf("Password authetications : validate password")
	if u.Password != in.Password {
		err = errors.New("auth failed")
		fmt.Printf("u.Password %s in.Password %s \n", u.Password, in.Password)
		return
	}

	//Auth Successful
	err = nil
	return
}
