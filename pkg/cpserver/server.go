package cpserver

import (
	"context"
	"errors"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	cp "gitlab.com/devesh25427/authorization/apis/authorization/v2"
	"gitlab.com/devesh25427/authorization/pkg/authn/password"
	_ "gitlab.com/devesh25427/authorization/pkg/authn/password"
	"gitlab.com/devesh25427/authorization/pkg/resources"
	"gitlab.com/devesh25427/authorization/pkg/users"
)

//Server : Server struct
type Server struct {
}

func NewServer() (s *Server, err error) {
	s = &Server{}
	return
}

//Authenticate user
func (s Server) LoginUser(ctx context.Context, req *cp.LoginRequest) (resp *cp.LoginResponse, err error) {
	fmt.Printf("LoginUser called \n")
	var u *users.User
	fmt.Printf("username is %s password is %s \n", req.UserName, req.Password)
	if u, err = users.GetUser(&users.User{UserName: req.UserName}); err != nil {
		err = errors.New("user not found")
		return
	}
	if u.AuthMethod != users.AUTH_METHOD_PASSWORD {
		err = errors.New("only password supported")
		return
	}

	in := password.PasswordAuthInput{Password: u.Password}
	var auth password.PasswordAuth
	if err = auth.AuthenticateUser(u, in); err != nil {
		err = errors.New("authentication failed")
		return
	}
	resp = &cp.LoginResponse{}
	return
}

func (s Server) AddUser(cts context.Context, req *cp.AddUserRequest) (resp *empty.Empty, err error) {
	fmt.Printf("AddUser called \n")
	var u *users.User
	if u, err = users.NewUser(req.UserName, int(req.AuthMethod), req.Password); err != nil {
		return
	}
	users.AddUser(u)
	resp = &empty.Empty{}
	err = nil
	users.ConvertToJson()
	fmt.Printf("AddUser Success \n")
	return
}

//Delete user
func (s Server) DeleteUser(ctx context.Context, req *cp.DeleteUserRequest) (resp *empty.Empty, err error) {
	fmt.Printf("DeleteUser called \n")
	if err = users.DeleteUser(&users.User{UserName: req.UserName}); err != nil {
		return
	}

	err = nil
	users.ConvertToJson()
	fmt.Printf("DeleteUser Success \n")
	return
}

func (s Server) GetResource(ctx context.Context, req *cp.GetResourceRequest) (resp *cp.GetResourceResponse, err error) {
	fmt.Printf("GetResource called \n")
	resp = &cp.GetResourceResponse{}
	return
}

func (s Server) AddResource(ctx context.Context, req *cp.AddResourceRequest) (resp *empty.Empty, err error) {

	fmt.Printf("AddResource called \n")
	var r *resources.Resource
	//list of users
	var u []string
	for _, s := range req.AllowedUsers {
		u = append(u, s.UserName)
	}
	if r, err = resources.NewResource(req.ResourceId, u, req.Value); err != nil {
		return
	}
	if err = resources.AddRes(r); err != nil {
		return
	}
	resp = &empty.Empty{}
	err = nil
	resources.ConvertToJson()
	fmt.Printf("AddUser Success \n")
	return
}

func (s Server) UpdateResource(ctx context.Context, req *cp.UpdateResourceRequest) (resp *empty.Empty, err error) {
	resp = &empty.Empty{}
	fmt.Printf("UpdateResource called \n")
	return
}

// Create new resource
func (s Server) DeleteResource(ctx context.Context, req *cp.DeleteResourceRequest) (resp *empty.Empty, err error) {
	resp = &empty.Empty{}
	fmt.Printf("DeleteResource called \n")
	return
}

//Authenticate interface
type Authenticate interface {
	AuthenticateUser(u *users.User, i interface{}) (err error)
}
