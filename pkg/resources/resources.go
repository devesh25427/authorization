package resources

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"

	"gitlab.com/devesh25427/authorization/pkg/users"
)

//Implements resources

type Resource struct {
	Name string `json:"res_name"`
	//List of allowed users
	AllowedUsers  []string `json:"allowed_users,omitempty"`
	ResourceValue string   `json:"resource_value"`
}

func NewResource(n string, u []string, v string) (res *Resource, err error) {
	if n == "" {
		err = errors.New("name is empty")
		return
	}

	var allowedUsers []string
	//Check valid users
	for _, s := range u {
		if _, err = users.GetUser(&users.User{UserName: s}); err != nil {
			allowedUsers = append(allowedUsers, s)
		}
	}
	res = &Resource{Name: n, AllowedUsers: allowedUsers, ResourceValue: v}
	return
}

//Manage list of users
var reslist []Resource

//Add user to list
func AddRes(u *Resource) (err error) {
	if u == nil {
		err = errors.New("invalid resource")
		return
	}
	for _, s := range reslist {
		if u.Name == s.Name {
			err = errors.New("duplicate resource")
			return
		}
	}
	reslist = append(reslist, *u)
	return
}

//Add user to list
func DeleteRes(u *Resource) (err error) {
	if u == nil {
		err = errors.New("invalid resource")
		return
	}
	for i, s := range reslist {
		if u.Name == s.Name {
			//Remove element found
			reslist = append(reslist[:i], reslist[i+1:]...)
			return
		}
	}
	err = errors.New("not found")
	return
}

//Add user to list
func GetUser(u *Resource) (r *Resource, err error) {
	if u == nil {
		err = errors.New("invalid resource")
		return
	}
	for _, s := range reslist {
		if u.Name == s.Name {
			r = &Resource{Name: u.Name, ResourceValue: u.ResourceValue, AllowedUsers: u.AllowedUsers}
			return
		}
	}
	err = errors.New("not found")
	return
}

//Convert list to Json
func ConvertToJson() (j string, err error) {

	out, err := json.MarshalIndent(reslist, "", "  ")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	j = string(out)
	fmt.Printf("Resource list is %s \n", j)
	return
}
