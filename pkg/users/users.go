package users

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
)

//This package implementes user management

const (
	AUTH_METHOD_PASSWORD = 0
	AUTH_METHOD_OIDC     = 1
)

type User struct {
	UserName   string `json:"user_name"`
	AuthMethod int    `json:"auth_method"`
	Password   string `json:"password,omitempty"`
}

func NewUser(name string, method int, p string) (u *User, err error) {
	if name == "" || (method != AUTH_METHOD_OIDC && method != AUTH_METHOD_PASSWORD) {
		err = errors.New("invalid input")
		return
	}
	if method == AUTH_METHOD_PASSWORD && p == "" {
		err = errors.New("password can't be null")
		return
	}

	u = &User{
		UserName:   name,
		AuthMethod: method,
		Password:   p,
	}

	return
}

//Manage list of users
var userlist []User

//Add user to list
func AddUser(u *User) (err error) {
	if u == nil {
		fmt.Printf("Invalid user")
	}
	for _, s := range userlist {
		if u.UserName == s.UserName {
			err = errors.New("duplicate user")
			return
		}
	}
	userlist = append(userlist, *u)
	return
}

//Add user to list
func DeleteUser(u *User) (err error) {
	if u == nil {
		fmt.Printf("Invalid user")
	}
	for i, s := range userlist {
		if u.UserName == s.UserName {
			//Remove element found
			userlist = append(userlist[:i], userlist[i+1:]...)
			return
		}
	}
	err = errors.New("user not found")
	return
}

//Add user to list
func GetUser(u *User) (user *User, err error) {
	if u == nil {
		fmt.Printf("Invalid user")
	}
	for _, s := range userlist {
		if u.UserName == s.UserName {
			user = &User{UserName: u.UserName, Password: u.Password, AuthMethod: u.AuthMethod}
			return
		}
	}
	err = errors.New("not found")
	return
}

//Convert list to Json
func ConvertToJson() (j string, err error) {

	out, err := json.MarshalIndent(userlist, "", "  ")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	j = string(out)
	fmt.Printf("User list is %s \n", j)
	return
}
